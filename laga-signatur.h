#ifndef LAGA_SIGNATUR_H
#define LAGA_SIGNATUR_H

#include <string>
#include <iostream>
#include <vector>
#include <fstream>

struct law_entry {
	std::string name;
	std::string url;

	friend std::ostream &operator<< (std::ostream &, const law_entry &);
};


struct signature_message {
	std::string username;
	std::string association;
	std::string title;
	law_entry law;

	friend std::ostream &operator<< (std::ostream &, const signature_message &);
};

std::vector<law_entry> load_lagar(
		std::ifstream &law_file,
		const std::string &filename);

law_entry &random_law(std::vector<law_entry> entries);

#endif /* LAGA_SIGNATUR_H */

#include "laga-signatur.h"

#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <vector>
#include <format>

#include "translations.h"

const char US = 0x1F; /* ASCII Unit Separator */

template<>
struct std::formatter<law_entry> : std::formatter<std::string> {
	auto format(law_entry entry, format_context &ctx) const {
		return formatter<std::string>::format(
			std::format("{} <{}>", entry.name, entry.url),
			ctx
		);
	}
};

std::ostream &operator<< (std::ostream &out, const law_entry &entry) {
	out << std::format("{}", entry);
	return out;
}

std::ostream &operator<< (std::ostream &out, const signature_message &msg) {
	out << msg.username << "\n";
	if (! msg.title.empty()) {
		out << msg.title << "\n";
	}

	out
		<< "\n"
		<< std::vformat(
				/** First argument is name of association, second
				 * argument is the law being followed */
				_("{0} follows {1}"),
				std::make_format_args(msg.association, msg.law)
			);
	return out;
}

std::vector<law_entry> load_lagar(
		std::ifstream &law_file,
		const std::string &filename)
{
	std::vector<law_entry> entries;

	std::string line;
	while (true) {
		std::getline(law_file, line);
		if (law_file.bad()) {
			throw std::runtime_error(_("Failed reading line from file: ") + filename);
		}
		if (law_file.eof()) break;
		if (line.empty()) break;
		// law_file.exceptions(law_file.failbit);

		size_t delim = line.find(US);
		if (delim == std::string::npos) continue;

		law_entry entry {
			.name = line.substr(0, delim),
			.url = line.substr(delim + 1),
		};

		entries.push_back(entry);
	}

	return entries;
}

template <typename T>
static T &rand_choice(std::vector<T> &v) {
	std::random_device rd;
	std::uniform_int_distribution<size_t> dist(0, v.size());
	return v[dist(rd)];
}

law_entry &random_law(std::vector<law_entry> entries) {
	return rand_choice(entries);
}

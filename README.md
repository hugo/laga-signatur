Building
--------

Dependencies
------------
- boost-libs
- libxdg-basedir

Dev Dependencies
----------------
- boost development libraries
- A C++ compiler
- cmake

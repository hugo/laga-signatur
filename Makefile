.PHONY: all update-po

all:
	@echo 'Use CMake to actually build this project'
	@echo 'This makefile however sets up translation sources'
	@echo 'Please see Makefile for details.'

po/laga-signatur.pot:
	xgettext --keyword=_ --language=c++ --from-code=utf-8 --add-comments -o $@ *.cpp

po/%/laga-signatur.po: po/laga-signatur.pot
	mkdir -p $(dir $@)
	msginit --input=$< --locale=$(basename $(dir $@)) --output=$@

update-po:
	$(MAKE) -B po/laga-signatur.pot
	for f in po/sv/laga-signatur.po; do \
		msgmerge --update "$$f" po/laga-signatur.pot; \
	done

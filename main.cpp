#include <iostream>
#include <string>

#include <boost/program_options.hpp>

#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>
#include <clocale>
#include <format>
#include <basedir.h> /* XDG basedir */

#include "laga-signatur.h"
#include "translations.h"

using namespace boost::program_options;

int main(int argc, const char *argv[]) {
	std::setlocale (LC_ALL, "");
	setup_translations();

	try {
		options_description desc{ _("Available options") };
		desc.add_options()
			("help,h", _("Display this help"))
			("version", _("Display program version"))
			("name,n", value<std::string>(), 
			 _("Set name for signature, defaults to the comment field"
				 " of the currently logged in user."))
			("title,t", value<std::string>()->default_value(""), 
			 _("Sets the position line of the signature, such as"
				 " \"Chairman of the Board\". If unset, the position line"
				 " will be omitted"))
			("association,a", value<std::string>()->default_value("Lysator"), 
			 _("Name of the association which follows laws."))
			("file,f", value<std::string>(), 
			 _("File to read laws from."));

		variables_map vm;
		store(parse_command_line(argc, argv, desc), vm);
		notify(vm);

		std::string title, username, association, filename;
		std::ifstream law_file;

		if (vm.count("help")) {
			std::cout << desc << '\n';
			return 0;
		}

		if (vm.count("version")) {
			std::cout << std::format("{} (build {})\n",
					PROGRAM_VERSION,
					BUILD_DATE);
			return 0;
		}

		if (vm.count("title")) {
			title = vm["title"].as<std::string>();
		}

		if (vm.count("name")) {
			username = vm["name"].as<std::string>();
		} else {
			username = getpwuid(geteuid())->pw_gecos;
		}

		if (vm.count("association")) {
			association = vm["association"].as<std::string>();
		}

		if (vm.count("file")) {
			filename = vm["file"].as<std::string>();

			law_file.open(filename, std::ios::binary);
			if (! law_file.is_open()) {
				throw std::runtime_error(_("Failed to open file: ") + filename);
			}

		} else {
			xdgHandle handle;
			xdgInitHandle(&handle);
			for (
					const char *const *data_dirs = xdgSearchableDataDirectories(&handle); 
					*data_dirs != NULL; 
					data_dirs++
				) 
			{
				filename = std::string(*data_dirs) + "/laga-signatur/lagar.data";
				law_file.open(filename, std::ios::binary);
				if (law_file.is_open()) {
					break;
				}
			}
			if (! law_file.is_open()) {
				throw std::runtime_error(_("Failed to find any data file."));
			}
			xdgWipeHandle(&handle);
		}

		auto entries = load_lagar(law_file, filename);
		auto law = random_law(entries);

		signature_message msg {
			.username = username,
			.association = association,
			.title = title,
			.law = law,
		};

		std::cout << msg << "\n";

	} catch (const std::ios_base::failure &fail) {
		std::cerr << fail.what() << "\n";
	} catch (const std::runtime_error &err) {
		std::cerr << err.what() << "\n";
	} catch (const error &ex) {
		std::cerr << ex.what() << "\n";
	}
}

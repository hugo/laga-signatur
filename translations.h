#ifndef TRANSLATIONS_H
#define TRANSLATIONS_H

#include <libintl.h>

#define _(s) gettext(s)

void setup_translations();

#endif /* TRANSLATIONS_H */
